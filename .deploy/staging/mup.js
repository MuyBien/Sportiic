module.exports = {
  servers: {
    one: {
      host: '79.137.36.221',
      username: 'root',
      // pem: '/home/user/.ssh/id_rsa',
      password: 'smtp:mail',
      // or leave blank to authenticate using ssh-agent
    }
  },

  meteor: {
    name: 'sportiic',
    path: '../../../Sportiic',
    // port: 000, // useful when deploying multiple instances (optional)
    /*volumes: { // lets you add docker volumes (optional)
      "/host/path": "/container/path", // passed as '-v /host/path:/container/path' to the docker run command
      "/second/host/path": "/second/container/path"
    },*/
    docker: {
      //image: 'kadirahq/meteord', // (optional)
      image: 'abernix/meteord:base', // use this image if using Meteor 1.4+
      /*args: [ // lets you add/overwrite any parameter on the docker run command (optional)
        "--link=myCustomMongoDB:myCustomMongoDB", // linking example
        "--memory-reservation 200M" // memory reservation example
      ],*/
      // (optional) Only used if using your own ssl certificates. Default is "meteorhacks/mup-frontend-server"
      //imageFrontendServer: 'meteorhacks/mup-frontend-server'
    },
    servers: {
      one: {} // list of servers to deploy, from the 'servers' list
    },
    buildOptions: {
      serverOnly: true, // skip building mobile apps, but still build the web.cordova architecture
      debug: true,
      cleanAfterBuild: true, // default
      //buildLocation: '/my/build/folder', // defaults to /tmp/<uuid>

      //set serverOnly: false if want to build mobile apps when deploying

      // Remove this property for mobileSettings to use your settings.json. (optional)
      /*mobileSettings: {
        yourMobileSetting: "setting value"
      },
      server: 'http://app.com', // your app url for mobile app access (optional)
      allowIncompatibleUpdates: true, //adds --allow-incompatible-updates arg to build command (optional)
      */
    },
    env: {
      ROOT_URL: 'https://dev.sportiic.com', // set to https to force redirect from http
      MONGO_URL: 'mongodb://localhost/meteor',
      MAIL_URL: 'smtp:mail',
      PORT: 80
    },
    /*log: { // (optional)
      driver: 'syslog',
      opts: {
        "syslog-address":'udp://syslogserverurl.com:1234'
      }
    },*/
    ssl: {
      // Enables let's encrypt (optional)
      autogenerate: {
        email: 'contact@sportiic.com',
        domains: 'dev.sportiic.com' // comma seperated list of domains
      }
    },
    deployCheckWaitTime: 120, // default 10

    // Shows progress bar while uploading bundle to server (optional)
    // You might need to disable it on CI servers
    enableUploadProgressBar: true // default false.
  }
};
