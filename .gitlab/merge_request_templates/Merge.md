## Status
Cette merge request est : (supprimer la ligne fausse)
- prête à être revue
- en cours de finalisation /wip

## Description
Quelques lignes pour décrire ce que fait cette merge request

## Tickets liés
Liste des tickets résolus par ce merge
- Fix #...

## Todos
- [ ] Tests fonctionnels
- [ ] Tests automatiques
- [ ] Documentation
- [ ] Release note
- [ ] Numéro de version changé sur le dashboard si besoin
- [ ] Script Migrations.js mis à jour si modification de base de données

## Impact de cette MR
Liste des fonctionnalités qui peuvent être impacté par cette MR

*
