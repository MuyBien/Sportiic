### RÉSUMÉ
(Résumez le bug rencontré de façon concise)

### ETAPES POUR REPRODUIRE
(Comment peut-on reproduire ce problème - ceci est très important)

### COMPORTEMENT ATTENDU
(Que devrait-il se passer normalement)

### COMPORTEMENT ACTUEL
(Que se passe-il maintenant)

### LOGS ET/OU SCREENSHOTS PERTINENTS
(Collez tous les logs pertinents - S'il vous plait, utilisez les blocs de ```code``` pour formater les messages de la console ou du code pour faciliter la lecture)

### SYSTÈME INFORMATIONS
Navigateur (Version) sur lequel vous avez rencontré le problème si relevant

### RÉSOLUTIONS POSSIBLES
(Si vous le pouvez, citez la ligne du code qui pourrait être responsable du problème)

/label ~bug
