import { Meteor } from 'meteor/meteor';

Template.registerHelper("userMail", function (userID) {
  var user = Meteor.users.findOne({ _id: userID });
  if(user.emails){
    return user.emails[0].address;
  }
  if (user.services.google !== undefined) {
    return user.services.google.email;
  }
  if (user.services.facebook !== undefined) {
    return user.services.facebook.email;
  }
});

Template.registerHelper("userName", function (userID) {
  var user = Meteor.users.findOne({ _id: userID });
  if(user.username){
    return user.username;
  }
  if (user.services.google !== undefined) {
    return user.services.google.given_name;
  }
  if (user.services.facebook !== undefined) {
    return user.services.facebook.name;
  }
});

Template.registerHelper("userAvatar", function (userID) {
    var user = Meteor.users.findOne({ _id: userID });
    if (user.profile.avatar.image !== undefined) {
      return "<div class='avatar imaged' style='background-image:url(\"" + user.profile.avatar.image + "\")'></div>";
    }
    if (user.services.google !== undefined) {
      return "<div class='avatar imaged' style='background-image:url(\"" + user.services.google.picture + "\")'></div>";
    }
    if (user.services.facebook !== undefined) {
      return "<div class='avatar imaged' style='background-image:url(\"http://graph.facebook.com/" + user.services.facebook.id + "/picture\")'></div>";
    }
    var username = user.username;
    return "<div class='avatar colored' style='background-color:" + user.profile.avatar + "'><span>" + username.substring(0, 1).toUpperCase() + username.substring(1, 2) + "</span></div>";
});

Template.registerHelper("displayDate", function (isoDate) {
  return moment(isoDate).calendar();
});

Template.registerHelper("displayDuration", function (frequency) {
  return moment.duration(frequency.number, frequency.type).humanize();
});

Template.registerHelper("isAdmin", function(userID, sessionID){
  let rolesManager = new RolesManager();
  return rolesManager.isUserInRole({ userID, role: 'admin', group: sessionID });
});

Template.registerHelper("isCaptain", function(userID, sessionID){
  let rolesManager = new RolesManager();
  if(rolesManager.isUserInRole({ userID, role: 'captain', group: sessionID })){
    return "selected";
  }
});

Template.registerHelper("isManager", function(userID, sessionID){
  let rolesManager = new RolesManager();
  return rolesManager.isUserInRole({ userID, role: ['admin', 'captain'], group: sessionID });
});

Template.registerHelper("isPlayer", function(userID, sessionID){
  let rolesManager = new RolesManager();
  if(!rolesManager.isUserInRole({ userID, role: 'admin', group: sessionID }) && !rolesManager.isUserInRole({ userID, role: 'captain', group: sessionID })){
    return "selected";
  }
});

Template.registerHelper("currentUserID", function(){
  return Meteor.userId();
});
