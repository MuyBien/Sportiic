import '/imports/ui/layouts/main/layout.js';
import '/imports/ui/layouts/subscribe/subscribe.js';

import '/imports/ui/components/sessionDates/sessionDates.js';

import '/imports/ui/pages/sessionDate/sessionDate.js';
import '/imports/ui/pages/share/share.js';
import '/imports/ui/pages/dashboard/dashboard.js';
import '/imports/ui/pages/newSession/newSession.js';
import '/imports/ui/pages/profile/profile.js';
import '/imports/ui/pages/about/about.js';

import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';

Router.configure({
  layoutTemplate: 'layout'
});
Router.route('/', {
  name: 'dashboard',
  waitOn: function(){
    this.subscribe( 'users.me' )
  },
  data: function(){}
});

// given a url like "/subscribe/HFHd5h346dhg"
Router.route('/subscribe/:_token', {
    name: 'session.subscribe',
    template: 'subscribe',
    layoutTemplate: 'subscribeLayout',
    waitOn: function () {
      var token = this.params._token;
      return [
        this.subscribe( 'sessions.subscribe', token),
        this.subscribe( 'sessionDates.subscribe', token),
        this.subscribe( 'users.all' )
      ]
    },
    data: function () {
      if (this.ready()) {
        var token = this.params._token;
        var sessionToDisplay = Sessions.findOne({ token: token });
        var sessionDate = SessionDates.findOne({ "sessionID": sessionToDisplay._id });
        sessionDate.infos = sessionToDisplay;
        return sessionDate;
      }
    }
});

// given a url like "/session/579a2d4478de5fea162e8cf5"
Router.route('/session/:_ID', {
    name: 'session.display',
    template: 'sessionDate',
    layoutTemplate: 'layoutHeaderTransparent',
    waitOn: function () {
      return [
        this.subscribe( 'sessions.all' ),
        this.subscribe( 'sessionDates.currents' ),
        this.subscribe( 'roles.all' ),
        this.subscribe( 'users.all' )
      ]
    },
    data: function () {
      if (this.ready()) {
        var sessionID = this.params._ID;
        var sessionToDisplay = Sessions.findOne({ _id: sessionID });
        var sessionDate = SessionDates.findOne({ "sessionID": sessionID });
        sessionDate.infos = sessionToDisplay;
        return sessionDate;
      }
    }
});

Router.route('/session/:_ID/share', {
    name: 'session.share',
    template: 'share',
    waitOn: function () {
      return [
        this.subscribe( 'sessions.all' ),
        this.subscribe( 'sessionDates.currents' ),
        this.subscribe( 'roles.all' ),
        this.subscribe( 'users.all' )
      ]
    },
    data: function () {
      if (this.ready()) {
        var sessionID = this.params._ID;
        var sessionToDisplay = Sessions.findOne({ "_id": sessionID });
        var sessionDate = SessionDates.findOne({ "sessionID": sessionID });
        sessionDate.infos = sessionToDisplay;
        return sessionDate;
      }
    }
});

// given a url like "/session/new"
Router.route('/new', {
    name: 'session.new',
    template: 'newSession',
    data: function () {}
});

Router.route('/profile/me', {
    name: 'profile.me',
    template: 'profile',
    waitOn: function () {
      return [
        this.subscribe( 'users.me' )
      ]
    },
    data: function () { return Meteor.user() }
});

Router.route('/about', {
    name: 'about',
    template: 'about',
    data: function () {}
});

/** Before Actions **/
Router.onBeforeAction(function () {
  if(Meteor.user()){
    var sessionID = this.params._ID;
    var sessionToDisplay = Sessions.findOne({ _id: sessionID, subscribers: {$elemMatch: {userID: Meteor.userId()}} });
    if(!sessionToDisplay){
      Bert.alert({
        type: 'warning',
        message: "La session n'a pas été trouvée. Êtes-vous abonné à cette session ?"
      });
      this.redirect('/');
      return false;
    }
  }
  this.next();
}, { only: ['session.display', 'session.share'] });

Router.onAfterAction(function () {
    document.querySelector("body").scrollTop=0;
});
