import { Random } from 'meteor/random';

// Add Facebook configuration entry
ServiceConfiguration.configurations.update(
  { service: "facebook" },
  { $set: {
      appId: "822000564567020",
      secret: "ada42fb86303e019a3b9f67e02c840c0"
    }
  },
  { upsert: true }
);

// Add Google configuration entry
ServiceConfiguration.configurations.remove({ service: "google" });
ServiceConfiguration.configurations.update(
  { service: "google" },
  { $set: {
      clientId: "22230465669-1jps6n002o3i1m3fc8bhiklbia3oekf3.apps.googleusercontent.com",
      client_email: "contact@sportiic.com",
      secret: "T41pgd67qIKPga9VuX8RPbPs"
    }
  },
  { upsert: true }
);

Accounts.onCreateUser((options, user) => {
  if (user.profile == undefined) { user.profile = {} };
  user.profile.avatar = randomColor({ luminosity: 'dark' });
  return user;
});
