import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { getLastSessionDate } from '/imports/api/sessions/methods.js';
/**
Pour chaque session regarde si la derniere sessionDate est dépassé de 2h. Si oui, on en créé une nouvelle.
**/
SyncedCron.add({
  name: 'SessionsFrequency',
  schedule: function(parser) {
    return parser.text('every 1 hours');
  },
  job: function() {
    var allSessions = Sessions.find().fetch();
    allSessions.forEach( function(oneSession){

      var lastSessionDate = getLastSessionDate.call({ sessionID: oneSession._id });

      if(!lastSessionDate){ return; }
      var sessionDate = lastSessionDate.date;

      if( oneSession.frequency && moment(sessionDate).add(2, "hours").isBefore(moment()) ){
        var sessionID = oneSession._id;
        var nextDateAsMoment = moment(sessionDate).add(oneSession.frequency.number, oneSession.frequency.type).tz("Europe/Paris");
        var nextDate = new Date(nextDateAsMoment.toString()) ;

        var sessionNext = {
          sessionID: sessionID,
          date: nextDate,
          players: [],
          comments: [],
          last: true
        };

        var sessionDateID = SessionDates.insert( sessionNext );
        SessionDates.update(lastSessionDate._id, { $set: {"last": false} });

        console.log("Nouvelle date ajoutée (" + nextDate + ") pour la session " + oneSession.name);

        if(Meteor.isServer){
          let notification = new Notifier({
            sessionID: sessionID,
            message: "Rendez-vous le " + nextDateAsMoment.format("dddd Do MMMM à HH:mm") + " pour la prochaine session. Vous pouvez vous y inscrire dès à présent.",
            from: oneSession.name,
            notificationType: "sessionUpdate"
          });
          notification.send();
        }
      }
    });
  }
});

SyncedCron.start();
