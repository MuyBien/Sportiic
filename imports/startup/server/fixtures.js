import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { Random } from 'meteor/random';

var sessionJSON = {};
sessionJSON = JSON.parse( Assets.getText('datasets/session.json') );

faker.locale = "fr";

// if the database is empty on server start, create some sample data.
Meteor.startup(() => {

  const SESSION_COUNT = 3;
  const USERS_COUNT_BY_SESSION = 30;
  const SESSION_SPORTS = ["soccer", "basketball", "volleyball", "tennis", "walking", "climbing"];
  var adminId = Meteor.users.findOne({ "username": "Admin" })._id;

  if (!adminId) {
    console.log("Création de admin@sportiic.com");
    adminId = Accounts.createUser({
      "username": "Admin",
      "email" : "admin@sportiic.com",
      "password" : "notafmb"
    });
  }

  if(Sessions.find().count() === 0){
    console.log("Création de sessions");
    var randomUserIndex,
        sessionNext,
        nextDate,
        fakeLocation,
        sessionAddress,
        currentSubscribers = [],
        participantsCount,
        messagesCount;

    for (var sessionIndex = 0; sessionIndex < SESSION_COUNT; sessionIndex++) {
      currentSubscribers = [];
      sessionToInsert = sessionJSON;

      // Name & Description
      sessionToInsert.name = faker.lorem.words().join(" ");
      sessionToInsert.description = faker.lorem.paragraphs();

      // Sport
      sessionToInsert.sport = faker.random.arrayElement(SESSION_SPORTS);
      // Location
      fakeLocation = faker.address;
      sessionAddress = [fakeLocation.streetAddress(), fakeLocation.city(), fakeLocation.state()];
      sessionToInsert.location = {
        "name" : sessionToInsert.name,
    		"address" : sessionAddress.join(" "),
    		"lat" : fakeLocation.latitude(),
    		"long" : fakeLocation.longitude()
      };

      // frequency
      sessionToInsert.frequency.number = faker.random.number(30);

      // Players Limit
      sessionToInsert.playerLimit = faker.random.number(20);

      // Token
      sessionToInsert.token = Random.id();

      sessionToInsert.created_by = adminId;
      sessionToInsert.creation_date = faker.date.between(moment().format("MM/DD/YYYY"), moment().subtract(1, "year").format("MM/DD/YYYY"));

      console.log(" - Création de la session");
      insertedSessionId = Sessions.insert(sessionToInsert);

      // SessionDate
      console.log("   - Création de sessionDates");
      sessionNext = {
        sessionID: insertedSessionId,
        date: faker.date.between(moment().format("MM/DD/YYYY HH:00"), moment().add(1, "weeks").format("MM/DD/YYYY")),
        players: [],
        comments: [],
        cancelled: false,
        last: true
      };

      var sessionDateID = SessionDates.insert( sessionNext );

      // Subscribers
      Sessions.update(insertedSessionId, { $push: {"subscribers": { "userID": adminId, "date": faker.date.recent() }} });

      console.log("   - Création d'utilisateurs random comme subscribers");
      var randomUser;
      for (var i = 0; i < USERS_COUNT_BY_SESSION; i++) {
        randomUser = {
          "username": faker.internet.userName(),
          "email": faker.internet.email(),
          "password": faker.internet.password()
        };
        randomUser._id = Accounts.createUser(randomUser);
        Meteor.users.update(randomUser._id, {
            $set: {
                "profile": { "avatar": { "image": faker.internet.avatar() } }
            }
        });
        currentSubscribers.push(randomUser);
        Sessions.update(insertedSessionId, { $push: {"subscribers": { "userID": randomUser._id, "date": faker.date.recent() }} });
      }

      // Participants
      participantsCount = faker.random.number( currentSubscribers.length - 1 );
      for (var i = 0; i < participantsCount; i++) {
        SessionDates.update({ _id: sessionDateID },{
          $push: { players: { userID: currentSubscribers[i]._id, date: new Date() } }
        });
      }

      // Messages
      messagesCount = faker.random.number( sessionToInsert.playerLimit );
      for (var i = 0; i < messagesCount; i++) {
        SessionDates.update({ _id: sessionDateID }, {
          $push: { comments: {
            author: currentSubscribers[i]._id,
            text: faker.lorem.sentence(),
            date: new Date()
          } }
        });
      }

      console.log(" - Session " + sessionToInsert.name + " créée");
    }
  }
});
