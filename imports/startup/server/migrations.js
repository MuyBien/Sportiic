import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { Users } from '/imports/api/users/users.js';

Migrations.add({
  version: 1,
  name: 'Passage de la fréquence en millisecondes en un object {number, type}. Pour la version 0.4b',
  up: function() {
    var allSessions = Sessions.find().fetch();
    var days = 0;
    allSessions.forEach( function(oneSession){
      if(typeof oneSession.frequency == "number"){
        days = oneSession.frequency / 1000 / 60 / 60 / 24;
        Sessions.update({ "_id": oneSession._id }, { $set: {"frequency": {"number": days, "type": "days"}} });
      }
    });
  },
  down: function() {
    var allSessions = Sessions.find().fetch();
    var milliseconds = 0;
    allSessions.forEach( function(oneSession){
      if(typeof oneSession.frequency == "object"){
        milliseconds = oneSession.frequency.number * 1000 * 60 * 60 * 24;
        Sessions.update({ "_id": oneSession._id }, { $set: {"frequency": milliseconds} });
      }
    });
  }
});

Migrations.add({
  version: 2,
  name: 'Suppression des sessionDates du document de session. Pour la version 0.5b',
  up: function() {
    Sessions.update({}, { $unset: { "sessionDates": "" } }, { multi:true });
    SessionDates.update({}, { $set: { "last": false } }, { multi:true });
  }
});

Migrations.add({
  version: 3,
  name: 'Ajout du role de owner aux créateurs de session et role admin à moi :P. Pour la version 0.6b',
  up: function() {
    Roles.createRole('admin');
    Sessions.find().fetch().forEach( function(oneSession){
      Roles.addUsersToRoles(oneSession.created_by, ["admin"], oneSession._id);
    });

    var adminUser = Meteor.users.findOne({ "username": "Admin" });
    if(!adminUser){
      adminUser = Accounts.createUser({
        email: 'admin@sportiic.com',
        password: 'notafmb',
        username: 'Admin'
      });
      var adminUser = Meteor.users.findOne({ "username": "Admin" });
    }
    Roles.addUsersToRoles(adminUser._id, ["admin"]);
  }
});

Migrations.add({
  version: 4,
  name: 'Ajout des roles de capitaine et de joueur. Pour la version 0.6b',
  up: function() {
    Roles.createRole('captain');
    Roles.createRole('player');
  }
});

Meteor.startup(function() {
  Migrations.migrateTo('latest');
});
