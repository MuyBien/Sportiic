import './migrations.js';

import './accounts.js';
import './crons.js';
//import './moment.js';

/** Sessions **/
import '../../api/sessions/sessions.js';
import '../../api/sessions/methods.js';
import '../../api/sessions/server/publications.js';

/** SessionDates **/
import '../../api/sessionDates/sessionDates.js';
import '../../api/sessionDates/methods.js';
import '../../api/sessionDates/server/publications.js';

/** Users **/
import '../../api/users/users.js';
import '../../api/users/methods.js';
import '../../api/users/server/publications.js';

/** Managers **/
import '../../api/notifier/methods.js';
import '../../api/roles/methods.js';

/** mails **/
import '../../api/mails/manager.js';

/** fixtures **/
import './fixtures.js';
