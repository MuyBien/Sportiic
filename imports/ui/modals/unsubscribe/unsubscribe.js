import { Template } from 'meteor/templating';

import './unsubscribe.html';

import { Sessions } from '/imports/api/sessions/sessions.js';
import { insertComment } from '/imports/api/sessionDates/methods.js';
import { unsubscribe } from '/imports/api/sessions/methods.js';

Template.unsubscribeModal.events({
  'click .btn.unsubscribe': function (event, instance) {
    Router.go('dashboard', {});

    insertComment.call({ sessionDateID: this._id, comment: "a quitté la session" });
    unsubscribe.call({ sessionID: this.infos._id, userID: Meteor.userId() }, function(err, data){
      console.log(err);
      if(!err){
        Bert.alert({
          type: 'success',
          message: 'Vous n\'êtes plus abonné'
        });
      }
    });
  }
});

Template.unsubscribeModal.helpers({
  'isAdminUnsubscribing': function(){
    if(this.infos){
      let rolesManager = new RolesManager();
      return rolesManager.isUserInRole({ userID: Meteor.userId(), role: 'admin', group: this.infos._id });
    }
  },
  'futurAdmin': function(){
    let subscribersList = Sessions.findOne({"_id": this.infos._id}).subscribers;
    let otherSubscribers = _.without(subscribersList, _.findWhere(subscribersList, {
      userID: Meteor.userId()
    }));
    return otherSubscribers[0].userID;
  }
});
