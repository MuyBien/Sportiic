import { Template } from 'meteor/templating';

import './cancel.html';

import { cancelSessionDate } from '/imports/api/sessionDates/methods.js';

Template.cancelModal.events({
  'click .cancel-session': function (event, instance) {
    event.preventDefault();
    cancelSessionDate.call({ sessionDateID: this._id }, function(err, data){
      if(!err){
        Bert.alert({
          type: 'success',
          message: 'La prochaine session est annulée'
        });
      }
    });
  }
});
