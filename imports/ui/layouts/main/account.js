import { Template } from 'meteor/templating';

import './account.html';

Template.account.events({
  'click #account-update': function(event){
        event.preventDefault();
        Router.go('/profile/me');
  },
  'click #logout': function(event){
      event.preventDefault();
      Meteor.logout();
  }
});

Template.account.onCreated( function() {
  this.subscribe( 'users.me' );
});

Template.account.rendered = function(){
  if (!this.rendered){
    $('.dropdown-button').dropdown({
        belowOrigin: false
      }
    );
  }
};
