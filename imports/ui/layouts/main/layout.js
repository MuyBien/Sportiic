// All default imports for layout
import '/imports/ui/components/account/account.js';
import '/imports/ui/components/menu/menu.js';
import '/imports/ui/components/bartop/bartop.html';
import '/imports/ui/components/login/login.html'; //TODO Gerer login comme une page. Avec redirections.

import '/imports/ui/modals/unsubscribe/unsubscribe.js';
import '/imports/ui/modals/cancel/cancel.js';

import { Template } from 'meteor/templating';

import './layout.html';

Template.layout.rendered = function(){
  initSW();
};

Template.layoutHeaderTransparent.rendered = function(){
  const headerNav = document.querySelector("header");
  $(window).on("scroll", function() {
      if($(window).scrollTop() > 50) {
        headerNav.classList.add("colored");
      } else {
        headerNav.classList.remove("colored");
      }
  });
  initSW();
};

const initSW = function(){
  if ('serviceWorker' in navigator) {
    console.log('Service Worker is supported');
    navigator.serviceWorker.register('/sw.js').then(function(reg) {
    }).catch(function(err) {
      console.log(':^(', err);
    });
  }
};
