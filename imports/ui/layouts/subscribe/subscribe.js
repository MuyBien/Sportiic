import { Template } from 'meteor/templating';

import './subscribe.html';

import { Sessions } from '/imports/api/sessions/sessions.js';
import { subscribe, getLastSessionDate } from '/imports/api/sessions/methods.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { insertComment } from '/imports/api/sessionDates/methods.js';

Template.subscribe.events({
  'click #subscribe-confirm': function (event, instance) {
    subscribe.call({ sessionID: this.infos._id, userID: Meteor.userId() }, function(err, data){
        if(!err){
          $('#session-register').addClass("registered");
          Bert.alert({
            type: 'success',
            message: 'Vous êtes mainteannt abonné à la session. Vous pouvez vous inscrire pour la prochaine session'
          });

          getLastSessionDate.call({sessionID: data.sessionID}, function(err, data){
            insertComment.call({ sessionDateID: data._id, comment: "a rejoint la session" });
          });

          Router.go('/session/' + data.sessionID);
        } else if (err.error == "already-subscribed"){
          Router.go('/');
          Bert.alert({
            type: 'warning',
            message: "Vous êtes déjà abonné à cette session"
          });
        } else {
          Bert.alert({
            type: 'error',
            message: "Désolé, une erreur nous a empeché de vous abonner à la session."
          });
        }
      });
  },
  'click #subscribe-cancel': function (event, instance) {
    Bert.alert({
      type: 'warning',
      message: 'Vous ne vous êtes pas abonné à la session ' + this.infos.name
    });
    Router.go('/');
  }
});
