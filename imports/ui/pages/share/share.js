import { Template } from 'meteor/templating';

import Clipboard from 'clipboard';

import { inviteByMail, setRole } from '/imports/api/sessions/methods.js';
import { getUserName } from '/imports/api/users/methods.js';

import './share.html';

Template.share.rendered = function(){
  if (!this.rendered){
    Materialize.updateTextFields();
    var clipboard = new Clipboard('.btn.invite.link');
    clipboard.on('success', function(e) {
      $(e.trigger).addClass("active").children(".button-text").text("Copié !");
      Bert.alert({
        type: 'success',
        message: "Le lien a été copié vers votre presse-papier. Vous pouvez l'envoyer aux personnes de votre choix."
      });
    });
    clipboard.on('error', function(e) {
      $(e.trigger).addClass("active");
      Bert.alert({
        type: 'error',
        message: "Le lien n'a pas pu être copié. Appuyez sur CTRL+C pour le récuperer."
      });
    });
  }
};

Template.share.events({
  'click .invite.mail': function(event, instance){
    if($(".invite.mail").hasClass("active")){
      $(".invitation-mail").fadeOut();
      $(".invite.mail").removeClass("active");
    } else {
      $(".invitation-mail").fadeIn();
      $("#invitation-mails").focus();
      $(".invite.mail").addClass("active");
    }
  },
  'click .close-subscribers-list': function(event, instance){
    Router.go('session.display', {_ID: this.infos._id});
  },
  'click #send-invitation-mails': function(event, instance){
    var mails = $("#invitation-mails").val().split(",");
    inviteByMail.call({sessionID: this.infos._id, mails: mails}, function(err, data){
      if(!err){
        Bert.alert({
          type: 'success',
          message: 'Les inscriptions ont étées envoyé par mail.'
        });
        $("#invitation-mails").val("");
        $(".invite.mail").click();
      }
    });
  },
  'change .subscriber .user-role.select': function(event, instance){
    var subscriber = $(event.target).closest(".subscriber");
    var roleToGive = $(event.target).val();
    var userID = subscriber.attr("data-user-id");
    var sessionID = subscriber.attr("data-session-id");
    setRole.call({ sessionID: sessionID, userID: userID, role: roleToGive }, function(err, data){
      if(!err){
        const roleLabel = roleToGive === 'player' ? 'joueur' : 'capitaine';
        Bert.alert({
          type: 'success',
          message: getUserName.call({ userID: userID }) + ' est maintenant ' + roleLabel
        });
      }
    });
  }

});

Template.share.helpers({
  "reverse": function(){
    return this.infos.subscribers.reverse();
  },
  "isNotAdminConnected": function(sessionID){
    let rolesManager = new RolesManager();
    return !rolesManager.isUserInRole({ userID: Meteor.userId(), role: 'admin', group: sessionID });
  }
})
