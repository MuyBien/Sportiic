import { Template } from 'meteor/templating';

import '/imports/ui/components/sessionDateInfos/sessionDateInfos.js';
import '/imports/ui/components/sessionDatePlayers/sessionDatePlayers.js';
import '/imports/ui/components/sessionDateFlux/sessionDateFlux.js';
import './sessionDate.html';

import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { registerPlayer, unregisterPlayer, insertComment, cancelSessionDate, activateSessionDate } from '/imports/api/sessionDates/methods.js';
import { getLastSessionDate, unsubscribe } from '/imports/api/sessions/methods.js';

Template.sessionDate.rendered = function(){
  if (!this.rendered){
    $('.collapsible').collapsible({
      accordion : false
    });
    $('.dropdown-button').dropdown({});
    $('.modal').modal();
    Materialize.updateTextFields();
  }
};

Template.sessionDate.helpers({
  "isRegistered": function (user) {
    return SessionDates.find({ _id: this._id, "players.userID": user._id }).count() > 0 ? true : false;
  }
});

Template.sessionDate.events({

  'click .session-register.not-registered': function (event, instance) {
    event.preventDefault();
    registerPlayer.call({ sessionDateID: this._id }, function(err, data){
      if(!err){
        $('#session-register').addClass("registered");
        Bert.alert({
          type: 'success',
          message: 'Vous avez été inscrit à la prochaine session. ;)'
        });
      }
    });

  },

  'click .session-register.registered': function (event, instance) {
    event.preventDefault();

    var unregisterBtn = $(event.target).closest(".registered");
    if(unregisterBtn.attr("data-confirm-state")=="false"){
      unregisterBtn.toggleClass("mobile-confirm orange").html("Confirmer : désinscription");
      unregisterBtn.attr("data-confirm-state", "true");
      setTimeout(function(){
        unregisterBtn.toggleClass("mobile-confirm orange").html("<i class='material-icons'>close</i>");
        unregisterBtn.attr("data-confirm-state", "false");
      }, 5000);
      return false;
    }

    unregisterPlayer.call({ sessionDateID: this._id, userID: Meteor.userId() }, function(err, data){
      if(!err){
        $('#session-register').removeClass("registered");
        Bert.alert({
          type: 'warning',
          message: 'Vous ne participez plus à la prochaine session. :('
        });
      }
    });

  },

  'click .session-menu .share-session': function (event, instance) {
    event.preventDefault();
    Router.go('session.share', {_ID: this.infos._id});
  },

  'click .session-menu .cancel-session': function (event, instance) {
    event.preventDefault();
    cancelSessionDate.call({ sessionDateID: this._id }, function(err, data){
      if(!err){
        Bert.alert({
          type: 'success',
          message: 'La prochaine session est annulée'
        });
      }
    });
  },

  'click .session-menu .reactive-session': function (event, instance) {
    event.preventDefault();
    activateSessionDate.call({ sessionDateID: this._id }, function(err, data){
      if(!err){
        Bert.alert({
          type: 'success',
          message: 'La prochaine session est réouverte'
        });
      }
    });
  },

  'click .register-cancelled': function (event, instance){
    event.preventDefault();
    Bert.alert({
      type: 'warning',
      message: 'La session a été annulée. Vous ne pouvez pas vous inscrire.'
    });
  }


});
