import { Template } from 'meteor/templating';
import { updateUserNotifications } from '/imports/api/users/methods.js';

import './dashboard.html';

import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';

Template.mail_hint.helpers({
  "isMailNotificationSeen": function(){
    var currentUser = Meteor.user();
    return typeof currentUser.profile.notifications === "undefined";
  }
});

Template.mail_hint.events({
  'click #cancel_mails': function (event, instance) {
    updateUserNotifications.call({ type: "sessionUpdate", active: false });
  }
});
