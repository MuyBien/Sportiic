import { Template } from 'meteor/templating';
import { updateUserName, updateUserMail, updateUserNotifications } from '/imports/api/users/methods.js';

import './profile.html';

Template.profile.rendered = function(){
  if (!this.rendered){
    $('.collapsible-profil').collapsible({
      accordion : false
    });
    Materialize.updateTextFields();
  }
};

Template.profile.helpers({
  isSessionUpdateChecked: function(){
    var currentUser = Meteor.user();
    return currentUser.profile.notifications.sessionUpdate ? "checked" : false;
  },
  isPlayerAddChecked: function(){
    var currentUser = Meteor.user();
    return currentUser.profile.notifications.playerAdd ? "checked" : false;
  },
  isPlayerRemoveChecked: function(){
    var currentUser = Meteor.user();
    return currentUser.profile.notifications.playerRemove ? "checked" : false;
  },
  isMessageAddChecked: function(){
    var currentUser = Meteor.user();
    return currentUser.profile.notifications.messageAdd ? "checked" : false;
  },
});

Template.profile.events({
  'change #first_name': function (event, instance) {
    const username = event.target.value;

    var doc = {};
    doc.username = username;

    updateUserName.call(doc, (err) => {
      if (err && err.error == "username-exists") {
        Bert.alert({
          type: 'warning',
          message: 'Un utilisateur utilise déjà ce nom'
        });
      }
      if(!err) {
        Bert.alert({
          type: 'success',
          message: 'Nom modifié'
        });
      }
    });
  },
  'change #mail': function (event, instance) {
    const usermail = event.target.value;

    var doc = {};
    doc.usermail = usermail;

    updateUserMail.call(doc, (err) => {
      if (err && err.error == "usermail-exists") {
        Bert.alert({
          type: 'warning',
          message: 'Un utilisateur utilise déjà cet email'
        });
      }
      if(!err) {
        Bert.alert({
          type: 'success',
          message: 'Email modifié'
        });
      }
    });
  },
  'change .notification-control': function (event, instance) {
    var doc = {};
    doc.type = $(event.target).attr("data-notification");
    doc.active = event.target.checked;

    updateUserNotifications.call(doc, (err) => {
      if(!err) {
        Bert.alert({
          type: 'success',
          message: 'Paramètre modifié'
        });
      }
    });
  }

});
