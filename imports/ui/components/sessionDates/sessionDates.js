import { Template } from 'meteor/templating';

import './sessionDates.html';

import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';

Template.sessionDates.helpers({
  sessionDates() {
    var userSessionDates = [];

  	var sessionDatesSubscribed = SessionDates.find().fetch();
    sessionDatesSubscribed.forEach( function(oneSessionDate){
      oneSessionDate.infos = Sessions.findOne({ "_id": oneSessionDate.sessionID });
      userSessionDates.push(oneSessionDate);
    });

    return userSessionDates;
  }
});

Template.sessionDates.events({
  'click .session': function (event, instance) {
    Router.go('/session/' + this.infos._id);
    slideout.close();
  }
});
