import { Template } from 'meteor/templating';

import './sessionDatePlayers.html';

import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { registerPlayer, unregisterPlayer } from '/imports/api/sessionDates/methods.js';

Template.sessionDatePlayers.rendered = function(){
  if (!this.rendered){
    /** Progress Animation **/
    var sessionProgress = $(".session-progress-wrapper .session-progress");
    sessionProgress.addClass( sessionProgress.attr("data-progress-level") );
  }
};

Template.sessionDatePlayers.helpers({
  "isRegistered": function (user) {
    return SessionDates.find({ _id: this._id, "players.userID": user._id }).count() > 0 ? true : false;
  },
  "sessionProgress": function () {
    if(this.infos.playerLimit != "undefined"){
      var progessPercent = (this.players.length * 100 / this.infos.playerLimit);
      return progessPercent>100 ? 100 : progessPercent;
    }
    return "0";
  },
  "sessionProgressLevel": function () {
    if(this.infos.playerLimit != "undefined"){
      var progress = (this.players.length * 100 / this.infos.playerLimit);
      if(progress < 40){ return "progress-low"; }
      if(progress < 80){ return "progress-medium"; }
      if(progress < 100){ return "progress-high"; }
      if(progress >= 100){ return "progress-complete"; }
    }
    return "";
  },
  "waitingList": function(){
    return this.players.slice(this.infos.playerLimit, this.players.length).length > 0;
  },
  "numberOnPlayingList": function(){
    return this.players.length > this.infos.playerLimit ? this.infos.playerLimit : this.players.length;
  },
  "numberOnWaitingList": function(){
    return this.players.slice(this.infos.playerLimit, this.players.length).length;
  },
  "playersToLimit": function(players){
    if(this.infos.playerLimit){
      return players.slice(0, this.infos.playerLimit);
    }
    return players;
  },
  "playersOverLimit": function(players){
    if(this.infos.playerLimit){
      return players.slice(this.infos.playerLimit, players.length);
    }
    return [];
  }
});

Template.sessionDatePlayers.events({

  'click .session-register.not-registered': function (event, instance) {
    event.preventDefault();

    registerPlayer.call({ sessionDateID: this._id }, function(err, data){
      if(!err){
        $('#session-register').addClass("registered");
        Bert.alert({
          type: 'success',
          message: 'Vous avez été inscrit à la prochaine session. ;)'
        });
      }
    });

  },

  'click .session-register.registered': function (event, instance) {
    event.preventDefault();

    var unregisterBtn = $(event.target).closest(".registered");
    if(unregisterBtn.attr("data-confirm-state")=="false"){
      unregisterBtn.toggleClass("orange red").html("Confirmer : désinscription");
      unregisterBtn.attr("data-confirm-state", "true");
      setTimeout(function(){
        unregisterBtn.toggleClass("orange red").html("Se desinscrire");
        unregisterBtn.attr("data-confirm-state", "false");
      }, 5000);
      return false;
    }

    unregisterPlayer.call({ sessionDateID: this._id }, function(err, data){
      if(!err){
        $('#session-register').removeClass("registered");
        Bert.alert({
          type: 'warning',
          message: 'Vous ne participez plus à la prochaine session. :('
        });
      }
    });

  },

  'click .register-cancelled': function (event, instance){
    event.preventDefault();
    Bert.alert({
      type: 'warning',
      message: 'La session a été annulée. Vous ne pouvez pas vous inscrire.'
    });
  }

});
