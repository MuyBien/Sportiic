import { Template } from 'meteor/templating';

import './menu.html';

import '../account/account.html';
import '../sessionDates/sessionDates.html';

Template.menu.events({
  'click .session-new': function (event, instance) {
    Router.go('/new');
  }
});

Template.menu.onCreated( function() {
  this.subscribe( 'sessions.all' );
  this.subscribe( 'sessionDates.currents' );
});

Template.menu.rendered = function(){
  if (!this.rendered){
      //TODO
  }
};
