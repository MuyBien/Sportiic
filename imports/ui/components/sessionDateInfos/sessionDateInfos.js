import { Template } from 'meteor/templating';

import './sessionDateInfos.html';

import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';

Template.sessionDateInfos.rendered = function(){
  if (!this.rendered){
    // Nothing to say
  }
};

Template.sessionDateInfos.helpers({
  // Nothing to say
});

Template.sessionDateInfos.events({
  'click .session-infos-tab .collapsible-header': function(event, instance){
    /** Map **/
    var locationMap = $('#sessionLocation');
    if(locationMap.children().length == 0){
      var locationName = locationMap.attr("data-location-name");
      var locationLat = locationMap.attr("data-location-lat");
      var locationLong = locationMap.attr("data-location-long");

      L.Icon.Default.imagePath = '/packages/bevanhunt_leaflet/images/';
      var map = L.map('sessionLocation', {scrollWheelZoom: false}).setView({lon: locationLong, lat: locationLat},15).invalidateSize(true);
      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(map);
      marker = L.marker({lon: locationLong, lat: locationLat}).addTo(map);
      map.panTo({lon: locationLong, lat: locationLat});

      setTimeout(function(){
         map.invalidateSize();
      }, 10);
    }
  },

  'click .subscribers-list,.show-subscribers': function(event, instance){
    Router.go('session.share', {_ID: this.infos._id});
  }
});

Template.displaySubscribers.helpers({
  "lastSubscribers": function () {
    if(this.infos.subscribers.length > 10){
        return this.infos.subscribers.slice( this.infos.subscribers.length-10 ).reverse();
    }
    return this.infos.subscribers.reverse();
  },
  "remainingSubscribers": function () {
    return this.infos.subscribers.length-10 > 0 ? true : false;
  },
  "remainingSubscribersCount": function () {
    return this.infos.subscribers.length-10;
  },
});
