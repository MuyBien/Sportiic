import { Template } from 'meteor/templating';

import './sessionDateFlux.html';

import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { insertComment } from '/imports/api/sessionDates/methods.js';

Template.sessionDateFlux.rendered = function(){
  if (!this.rendered){
    /** Send message autoexpand **/
    $(document).one('focus.textarea', '.new-message-input', function(){
			var savedValue = this.value;
			this.value = '';
			this.baseScrollHeight = this.scrollHeight;
			this.value = savedValue;
      $("html, body").animate({ scrollTop: $(document).height()-$(window).height() }, 300);
		}).on('input.textarea', '.new-message-input', function(){
			var minRows = this.getAttribute('data-min-rows')|0,
				 rows;
			this.rows = minRows;
			rows = Math.ceil((this.scrollHeight - this.baseScrollHeight) / 17);
			this.rows = minRows + rows;
		});

    $('.new-message-input').keydown(function(event) {
        if (event.keyCode == 13 && ! event.shiftKey) {
            $(this.form).submit();
            return false;
         }
    });

    $("form.new-message").on("submit", function(){
      $('.new-message-input').attr("rows", 1);
    });
  }
};

Template.sessionDateFlux.helpers({
  // Nothing to say
});

Template.sessionDateFlux.events({
  'submit .new-message': function (event, instance) {
    event.preventDefault();
    const target = event.target;
    const text = target.messageText.value;

    if(text.length > 0){
      insertComment.call({ sessionDateID: this._id, comment: text });
      target.messageText.value = '';
    }

  }
});
