import { Sessions } from '../sessions/sessions.js';
import { getUserMail } from '../users/methods.js';

Notifier = class Notifier {

  constructor(doc) {
    this.session = Sessions.findOne({ _id: doc.sessionID });

    this.notificationType = doc.notificationType;
    this.message = doc.message;
    this.userID = doc.userID;
    this.date = new Date();
    this.read = false;
    this.from = doc.from;

    this.mails = this.getMailsToNotify();
  }

  getMailsToNotify(){
    const notificationType = this.notificationType;
    const usersToNotify = [];
    let userMail;
    this.session.subscribers.forEach( function(oneSubscriber){
      let oneUser = Meteor.users.findOne({ _id: oneSubscriber.userID });
      if(oneUser.profile.notifications && oneUser.profile.notifications[notificationType]){
        userMail = getUserMail.call({ userID: oneUser._id });
        usersToNotify.push(userMail);
      }
    });
    return usersToNotify;
  }

  send(){
    const notification = this;
    this.mails.forEach( function(oneUserMail){
      SSR.compileTemplate('mailNotif', Assets.getText('mails/notification.html'));
      var content = SSR.render('mailNotif', notification);
      console.log("Sending mail to " + oneUserMail);

      Meteor.defer(function(){
        Email.send({
          to: oneUserMail,
          from: notification.from + " <no-reply@sportiic.com>",
          subject: notification.getTitle(),
          html: content
        });
      });
    });
  }

  getTitle(){
      if(this.notificationType == "sessionUpdate"){
        return "Mise à jour de la session";
      }
      if(this.notificationType == "playerAdd"){
        return "Nouvel inscrit";
      }
      if(this.notificationType == "playerRemove"){
        return "Désinscription";
      }
      if(this.notificationType == "messageAdd"){
        return "Nouveau message";
      }
      if(this.notificationType == "sessionInvitation"){
        return "Invitation";
      }
  }

};
