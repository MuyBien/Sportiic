import { Sessions } from '../sessions/sessions.js';
import { SessionDates } from '../sessionDates/sessionDates.js';

import { isSubscribed } from '../sessions/methods.js';
import { isRegistered } from '../sessionDates/methods.js';

export const userSubscribedMixin = function(params) {
  if(params.sessionDateID){
    var sessionDate = SessionDates.findOne(params.sessionDateID);
    params.sessionID = sessionDate.sessionID;
  }
  var isUSerSubscribed = isSubscribed._execute({ userId: params.userID }, {
    sessionID: params.sessionID,
    userID: params.userID
  });
  if(!isUSerSubscribed){
    throw new Meteor.Error("not-subscribed",
      "No subscribtion find for this user/session.");
  }
};

export const userNotSubscribedMixin = function(params) {
  if(params.sessionDateID){
    var sessionDate = SessionDates.findOne(params.sessionDateID);
    params.sessionID = sessionDate.sessionID;
  }
  var isUSerSubscribed = isSubscribed._execute({ userId: params.userID }, {
    sessionID: params.sessionID,
    userID: params.userID
  });
  if(isUSerSubscribed){
    throw new Meteor.Error("already-subscribed",
      "Subscribtion find for this user/session.");
  }
};

export const userRegisteredMixin = function(context, args) {
  var isPlayerRegistered = isRegistered._execute(context, args);
  if(isPlayerRegistered){
    throw new Meteor.Error("already-registered",
      "User already registered.");
  }
};

export const userUnregisteredMixin = function(context, args) {
  var isPlayerRegistered = isRegistered._execute(context, args);
  if(!isPlayerRegistered){
    throw new Meteor.Error("already-unregistered",
      "User already unregistered.");
  }
};

export const cancelledSessionMixin = function(params){
  if( SessionDates.findOne({ _id: params.sessionDateID }).cancelled ){
    throw new Meteor.Error("sessionDate-cancelled",
      "The sessionDate is cancelled");
  }
};

export const notCancelledSessionMixin = function(params){
  if( !SessionDates.findOne({ _id: params.sessionDateID }).cancelled ){
    throw new Meteor.Error("sessionDate-cancelled",
      "The sessionDate is not cancelled");
  }
};
