
MailManager = class MailManager {

  constructor(doc) {
    this.title = doc.title;
    this.content = doc.content;
    this.template = doc.template;
    this.from = doc.from;

    this.mails = doc.mails;
  }

  send(){
    const mail = this;
    this.mails.forEach( function(oneMail){
      SSR.compileTemplate('mailContent', Assets.getText('mails/' + mail.template + '.html'));
      var content = SSR.render('mailContent', mail.content);
      console.log("Sending mail to " + oneMail);

      Meteor.defer(function(){
        Email.send({
          to: oneMail,
          from: mail.from + " <no-reply@sportiic.com>",
          subject: mail.title,
          html: content
        });
      });
    });
  }

};
