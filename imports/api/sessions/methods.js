import { Sessions } from '../sessions/sessions.js';
import { SessionDates } from '../sessionDates/sessionDates.js';
import { userNotSubscribedMixin, userSubscribedMixin } from '../mixins/mixins.js';
import { getUserName } from '../users/methods.js';
import { isRegistered, unregisterPlayer } from '../sessionDates/methods.js';

export const isSubscribed = new ValidatedMethod({
  name: 'session.isSubscribed',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    sessionID: { type: String },
    userID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ sessionID, userID }) {
    return Sessions.find({ _id: sessionID, "subscribers.userID": userID }).count() > 0;
  }
});

export const subscribe = new ValidatedMethod({
  name: 'session.subscribe',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    sessionID: { type: String },
    userID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ sessionID, userID }) {
    userNotSubscribedMixin({ sessionID: sessionID, userID: userID });
    Sessions.update({
      _id: sessionID
    },{
      $push: { subscribers: { userID: userID, date: new Date() }}
    });
    return { sessionID: sessionID };
  }
});

export const unsubscribe = new ValidatedMethod({
  name: 'session.unsubscribe',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    sessionID: { type: String },
    userID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ sessionID, userID }) {
    userSubscribedMixin({ sessionID: sessionID, userID: userID });

    const sessionDateID = getLastSessionDate.call({sessionID: sessionID})._id;
    if(isRegistered.call({sessionDateID: sessionDateID, userID: userID })){
      unregisterPlayer.call({sessionDateID: sessionDateID, userID: userID });
    }

    const rolesManager = new RolesManager();
    if( rolesManager.isUserInRole({ userID, role: 'admin', group: sessionID }) ) {

      let subscribersList = Sessions.findOne({"_id": sessionID}).subscribers;
      let otherSubscribers = _.without(subscribersList, _.findWhere(subscribersList, {
        userID: userID
      }));
      const futurAdmin = otherSubscribers[0].userID;

      rolesManager.removeRole({ userID, role: 'admin', group: sessionID });
      rolesManager.setRole({ userID: futurAdmin, role: 'admin', group: sessionID });
    }


    Sessions.update({
      _id: sessionID
    },{
      $pull: { subscribers: { userID: userID }}
    });
    return { sessionID: sessionID };
  }
});

export const getLastSessionDate = new ValidatedMethod({
  name: 'session.getLastSessionDate',
  validate: new SimpleSchema({
    sessionID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ sessionID }) {
    var sessionToDisplay = Sessions.findOne({ _id: sessionID });
    var sessionDate = SessionDates.findOne({ "sessionID": sessionToDisplay._id, last: true });
    sessionDate.infos = sessionToDisplay;

    return sessionDate;
  }
});

export const inviteByMail = new ValidatedMethod({
  name: 'session.inviteByMail',
  validate: new SimpleSchema({
    sessionID: { type: String },
    mails: { type: [String] }
  }).validator({ clean: true, filter: false }),
  run({ sessionID, mails }) {
    var content = Sessions.findOne({ _id: sessionID });
    content.inviteur = getUserName.call({ userID: Meteor.userId() });

    if(Meteor.isServer && !Meteor.isTest){
      let mailManager = new MailManager({
        title: "Invitation : " + content.name,
        content: content,
        template: "invitation",
        from: content.name,
        mails: mails
      });
      mailManager.send();
    }

    return true;
    }
  });

  export const setRole = new ValidatedMethod({
    name: 'session.setRole',
    mixins : [LoggedInMixin],
    checkLoggedInError: {
      error: 'notLogged'
    },
    validate: new SimpleSchema({
      sessionID: { type: String },
      userID: { type: String },
      role: { type: String, allowedValues: ['captain', 'player'] }
    }).validator({ clean: true, filter: false }),
    run({ sessionID, userID, role }) {
      let rolesManager = new RolesManager();

      if( !rolesManager.isUserInRole({ userID: this.userId, role: 'admin', group: sessionID }) ) {
        throw new Meteor.Error('notAllowed', "You are not allowed to access this method");
      }
      if(this.userId === userID){
        throw new Meteor.Error('notAllowed', "You are not allowed to change your admin status");
      }

      rolesManager.setRole({ userID, role, group: sessionID });
    }
  });
