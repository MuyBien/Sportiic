import { Mongo } from 'meteor/mongo';

export const Sessions = new Mongo.Collection('sessions');

Sessions.publicFields = {
  "_id": 1,
  "name" : 1,
	"description" : 1,
	"sport" : 1,
	"location" : 1,
	"frequency" : 1,
	"playerLimit" : 1,
	"token" : 1,
	"subscribers" : 1,
	"created_by" : 1,
	"creation_date" : 1,
	"modified_by" : 1,
	"modification_date" : 1
};

Factory.define('session', Sessions, {
  name: "A session",
  description(){ return faker.lorem.paragraph(); },
  sport: "soccer",
  location: {},
  frequency: {},
  playerLimit: 10,
  token: "token",
  subscribers: [],
  sessionDates: [],
  created_by: "",
  creation_date : 1,
  modified_by : 1,
	modification_date : 1
});
