import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { isSubscribed, subscribe, getLastSessionDate, inviteByMail, setRole } from './methods.js';
import '../../api/roles/methods.js';
import { Random } from 'meteor/random';

import '../../api/sessions/server/publications.js';
import '../../api/sessions/methods.js';

if (Meteor.isServer) {

  import './server/publications.js';

  describe('sessions', function () {

    describe('mutators', function () {
      it('builds correctly from factory', function () {
        const session = Factory.create('session');
        assert.typeOf(session, 'object');
      });
    });

    describe('publications', function () {

      const userId = Random.id();
      const createSession = (props = {}) => {
        const session = Factory.create('session', props);
        _.times(3, () => {
          Factory.create('session', {});
        });
      };

      before(function () {
        Sessions.remove({});
        _.times(3, () => createSession({ subscribers: [{ "userID": userId }] }));
        _.times(1, () => createSession({ token: "fake_token" }));
      });

      it('send only public fields', function (done) {
        var sessionsCollector = new PublicationCollector({ userId: userId });
        sessionsCollector.collect('sessions.all', (collections) => {
          expect(collections.sessions[0]).to.have.all.keys(Sessions.publicFields);
          done();
        });
      });

      describe('sessions.all', function () {
        it('sends all subscribed sessions', function (done) {
          var sessionsCollector = new PublicationCollector({ userId });
          sessionsCollector.collect('sessions.all', (collections) => {
            assert.equal(collections.sessions.length, 3);
            done();
          });
        });

        it('does not send any public session (no userId)', function (done) {
          var sessionsCollector = new PublicationCollector();
          sessionsCollector.collect('sessions.all', (collections) => {
            assert.equal(collections.sessions.length, 0);
            done();
          });
        });

        it('does not send any session if not subscribed', function (done) {
          var sessionsCollector = new PublicationCollector({ userId: Random.id() });
          sessionsCollector.collect('sessions.all', (collections) => {
            assert.equal(collections.sessions.length, 0);
            done();
          });
        });
      });

      describe('sessions.subscribe', function () {
        it('sends the token related session', function (done) {
          var sessionsCollector = new PublicationCollector({ userId: Random.id() });
          sessionsCollector.collect('sessions.subscribe', "fake_token", (collections) => {
            assert.equal(collections.sessions.length, 1);
            done();
          });
        });

        it('does not send any session with random token', function (done) {
          var sessionsCollector = new PublicationCollector({ userId: Random.id() });
          sessionsCollector.collect('sessions.subscribe', faker.random.word, (collections) => {
            assert.equal(collections.sessions.length, 0);
            done();
          });
        });
      });

    });

    describe('methods', function () {
      const userId = Random.id();
      const playerId = Random.id();
      const sessionId = Random.id();
      const sessionDateId = Random.id();
      const createSession = (props = {}) => {
        const session = Factory.create('session', props);
        _.times(3, () => {
          Factory.create('session', {});
        });
      };

      before(function () {
        Sessions.remove({});
        SessionDates.remove({});
        _.times(1, () => createSession({ "_id": sessionId, subscribers: [{ "userID": userId }, { "userID": playerId }] }));
        Factory.create('sessionDate', { "_id": sessionDateId, "sessionID": sessionId, last: true });
        Roles.createRole('admin');
        Roles.createRole('captain');
      });

      describe('isSubscribed', function () {
        it('return true if an user is subscribed', function (done) {
          const context = { userId : userId };
          const args = { sessionID: sessionId, userID: userId };
          var isUserSubscribed = isSubscribed._execute(context, args);
          assert.isTrue(isUserSubscribed);
          done();
        });

        it('return false if an user is not subscribed', function (done) {
          const context = { userId : userId };
          const args = { sessionID: sessionId, userID: Random.id() };
          var isUserSubscribed = isSubscribed._execute(context, args);
          assert.isFalse(isUserSubscribed);
          done();
        });

        it('return false if an user is subscribed to another session', function (done) {
          var newUserId = Random.id();
          createSession({ "_id": Random.id(), subscribers: [{ "userID": newUserId }] });

          const context = { userId : userId };
          const args = { sessionID: sessionId, userID: newUserId };
          var isUserSubscribed = isSubscribed._execute(context, args);
          assert.isFalse(isUserSubscribed);
          done();
        });
      });

      describe('subscribe', function () {
        it('subscribe new user', function (done) {
          const userToSubscribe = Random.id();
          const context = { userId : userId };
          const args = { sessionID: sessionId, userID: userToSubscribe };
          subscribe._execute(context, args);
          var isUserSubscribed = isSubscribed._execute(context, args);
          assert.isTrue(isUserSubscribed);
          done();
        });

        it('do not subscribe an already subscribed user', function (done) {
          const context = { userId : userId };
          const args = { sessionID: sessionId, userID: userId };
          assert.throws( function(){
            subscribe._execute(context, args);
          }, Error, "already-subscribed");
          done();
        });
      });

      describe('getLastSessionDate', function(){
        it('return the last sessionDate', function(done){
          const context = { userId : userId };
          const args = { sessionID: sessionId };
          const lastSessionDate = getLastSessionDate._execute(context, args);
          assert.isTrue(lastSessionDate._id === sessionDateId);
          done();
        });
      });

      describe('inviteByMail', function(){
        // Ticket #36
      });

      describe('setRole', function(){

        it('throws an error if the user is not logged', function(done){
          const args = { sessionID: sessionId, userID: userId, role: 'captain' };
          assert.throws( function(){
            setRole._execute({}, args);
          }, Error, "notLogged");
          done();
        });

        it('throws an error if is not called with good parameters', function(done){
          const context = { userId : userId };

          let args = { sessionID: sessionId, userID: userId };
          assert.throws( function(){
            setRole._execute(context, args);
          }, Error, "Role is required [validation-error]");

          args = { sessionID: sessionId, role: 'captain' };
          assert.throws( function(){
            setRole._execute(context, args);
          }, Error, "User id is required [validation-error]");

          args = { userID: userId, role: 'captain' };
          assert.throws( function(){
            setRole._execute(context, args);
          }, Error, "Session id is required [validation-error]");

          args = { sessionID: sessionId, userID: userId, role: 'test' };
          assert.throws( function(){
            setRole._execute(context, args);
          }, Error, "test is not an allowed value [validation-error]");

          done();
        });

        /* Ne marche pas :(
        it('set the new role', function(done){
          const adminUserId = Accounts.createUser({
            email: 'admin@testing',
            password: 'testAdmin',
            username: 'testAdmin'
          });
          Roles.addUsersToRoles(adminUserId, 'admin', sessionId);

          const context = { userId : adminUserId };
          const args = { sessionID: sessionId, userID: playerId, role: 'captain' };

          setRole._execute(context, args);
          assert.isTrue( Roles.userIsInRole( playerId, 'captain', sessionId ) );
          done();
        });*/

        it('cannot be called by a non-admin of the session', function(done){
          const context = { userId : playerId };
          const args = { sessionID: sessionId, userID: playerId, role: 'captain' };

          assert.throws( function(){
            setRole._execute(context, args);
          }, Error, "You are not allowed to access this method [notAllowed]");

          done();
        });

        it('do not change the admin role', function(done){
          const adminUserId = Accounts.createUser({
            email: 'admin2@testing',
            password: 'testAdmin',
            username: 'testAdmin2'
          });
          Roles.addUsersToRoles(adminUserId, 'admin', sessionId);

          const context = { userId : adminUserId };
          const args = { sessionID: sessionId, userID: adminUserId, role: 'captain' };

          assert.throws( function(){
            setRole._execute(context, args);
          }, Error, "You are not allowed to change your admin status [notAllowed]");

          done();
        });
      });

    });

  });

}
