import { Sessions } from '../sessions.js';

// User session
Meteor.publish('sessions.all', function() {
  const userID = this.userId;

  const userSessions = Sessions.find({
  	"subscribers.userID": userID
  }, {
    fields: Sessions.publicFields,
  });

  if(userSessions){
    return userSessions;
  }
  return this.ready();
});

// Session Subscribe
Meteor.publish('sessions.subscribe', function(token) {
  const sessionToDisplay = Sessions.find({
  	token: token
  });

  if(sessionToDisplay){
    return sessionToDisplay;
  }
  return this.ready();
});
