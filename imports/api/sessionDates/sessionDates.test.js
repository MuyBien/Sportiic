import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { isRegistered, registerPlayer, unregisterPlayer, insertComment } from './methods.js';
import { Random } from 'meteor/random';

import '../../api/sessionDates/server/publications.js';
import './server/publications.js';

if (Meteor.isServer) {

  describe('sessionDates', function () {

    describe('mutators', function () {
      it('builds correctly from factory', function () {
        const sessionDate = Factory.create('sessionDate');
        assert.typeOf(sessionDate, 'object');
      });
    });

    describe('publications', function(){

      const userID = Random.id();
      const createSessionDate = (props = {}) => {
        var sessionAdded = Factory.create('session', { subscribers: [{ "userID": userID }], token: props.token });
        var sessionDateAdded = Factory.create('sessionDate', { sessionID: sessionAdded._id, last: props.last });
      };

      before(function () {
        SessionDates.remove({});
        _.times(3, () => createSessionDate({last: true}));
        _.times(6, () => createSessionDate({last: false}));
      });

      it('send only public fields', function(done){
        var sessionDatesCollector = new PublicationCollector({ userId: userID });
        sessionDatesCollector.collect('sessionDates.currents', (collections) => {
          expect(collections.sessionDates[0]).to.have.all.keys(SessionDates.publicFields);
          done();
        });
      });

      describe('sessionDates.currents', function () {
        it('sends all subscribed last sessionDates', function (done) {
          var sessionsCollector = new PublicationCollector({ userId: userID });
          sessionsCollector.collect('sessionDates.currents', (collections) => {
            assert.equal(collections.sessionDates.length, 3);
            done();
          });
        });

        it('does not send any public session (no userId)', function (done) {
          var sessionsCollector = new PublicationCollector();
          sessionsCollector.collect('sessionDates.currents', (collections) => {
            assert.equal(collections.sessionDates.length, 0);
            done();
          });
        });

        it('does not send any sessionDate if not subscribed', function (done) {
          var sessionsCollector = new PublicationCollector({ userId: Random.id() });
          sessionsCollector.collect('sessionDates.currents', (collections) => {
            assert.equal(collections.sessionDates.length, 0);
            done();
          });
        });
      });

      describe('sessionDates.subscribe', function () {

        const randomToken = Random.id();
        beforeEach(function () {
          createSessionDate({ token: randomToken, last: true });
        });

        it('sends the token related sessionDate', function (done) {
          var sessionsCollector = new PublicationCollector({ userId: Random.id() });
          sessionsCollector.collect('sessionDates.subscribe', randomToken, (collections) => {
            assert.equal(collections.sessionDates.length, 1);
            done();
          });
        });

      });
    });

    describe('methods', function(){

      const userID = Random.id();
      const registeredUserID = Random.id();
      const sessionDateID = Random.id();
      const createSessionDate = (props = {}) => {
        var sessionDateAdded = Factory.create('sessionDate', {
          _id: sessionDateID,
          players: [{ "userID": registeredUserID }]
        });
        var sessionAdded = Factory.create('session', {
          _id: sessionDateAdded.sessionID,
          subscribers: [{ userID: userID }, {userID: registeredUserID}],
          sessionDates: [{sessionDateID: sessionDateAdded._id}]
        });
      };

      before(function () {
        SessionDates.remove({});
        _.times(1, () => createSessionDate());
      });

      describe("isRegistered", function(){
        it("throws an error if not logged", function(done){
          const args = { sessionDateID: sessionDateID, userID: userID };
          assert.throws( function(){
            isRegistered._execute({}, args);
          }, Error, "notLogged");
          done();
        });

        it("return true if user is registered", function(done){
          const context = { userId : registeredUserID };
          const args = { sessionDateID: sessionDateID, userID: registeredUserID };
          var userRegistered = isRegistered._execute(context, args);
          assert.isTrue(userRegistered);
          done();
        });

        it("return false if user is not registered", function(done){
          const context = { userId : userID };
          const args = { sessionDateID: sessionDateID, userID: Random.id() };
          var userRegistered = isRegistered._execute(context, args);
          assert.isFalse(userRegistered);
          done();
        });
      });

      describe("registerPlayer", function(){
        it("throws an error if not logged", function(done){
          const args = { sessionDateID: sessionDateID };
          assert.throws( function(){
            registerPlayer._execute({}, args);
          }, Error, "notLogged");
          done();
        });

        it("throws an error if not subscribed to the session", function(done){
          const notSubscribedUserId = Random.id();
          const context = { userId : notSubscribedUserId };
          var args = { sessionDateID: sessionDateID };
          assert.throws( function(){
            registerPlayer._execute(context, args);
          }, Error, "not-subscribed");
          done();
        });

        it("register the current user", function(done){
          const context = { userId : userID };
          var args = { sessionDateID: sessionDateID };
          registerPlayer._execute(context, args);

          args = { sessionDateID: sessionDateID, userID: userID };
          var userRegistered = isRegistered._execute(context, args);
          assert.isTrue(userRegistered);

          done();
        });

        it("do not register the current user twice", function(done){
          const context = { userId : registeredUserID };
          var args = { sessionDateID: sessionDateID };

          assert.throws( function(){
            registerPlayer._execute(context, args);
          }, Error, "already-registered");
          done();
        });
      });

      describe("unregisterPlayer", function(){
        it("throws an error if not logged", function(done){
          const args = { sessionDateID: sessionDateID, userID: userID };
          assert.throws( function(){
            unregisterPlayer._execute({}, args);
          }, Error, "notLogged");
          done();
        });

        it("unregister the current user", function(done){
          const context = { userId : userID };
          var args = { sessionDateID: sessionDateID, userID: userID };
          unregisterPlayer._execute(context, args);

          args = { sessionDateID: sessionDateID, userID: userID };
          var userRegistered = isRegistered._execute(context, args);
          assert.isFalse(userRegistered);
          done();
        });

        it("do not unregister the current user twice", function(done){
          const context = { userId : userID };
          const args = { sessionDateID: sessionDateID, userID: userID };

          assert.throws( function(){
            unregisterPlayer._execute(context, args);
          }, Error, "already-unregistered");
          done();
        });
      });

      const assertCommentAdded = function(sessionDateID, text){
        var sessionDateCommented = SessionDates.findOne(sessionDateID);
        assert.isTrue( _.contains(_.pluck(sessionDateCommented.comments, 'text'), text) );
      };

      describe("insertComment", function(){
        it("throws an error if not logged", function(done){
          const args = { sessionDateID: sessionDateID, comment: faker.lorem.sentence() };
          assert.throws( function(){
            insertComment._execute({}, args);
          }, Error, "notLogged");
          done();
        });

        it("throws an error if not subscribed to the session", function(done){
          const notSubscribedUserId = Random.id();
          const context = { userId : notSubscribedUserId };
          var args = { sessionDateID: sessionDateID, comment: faker.lorem.sentence() };
          assert.throws( function(){
            insertComment._execute(context, args);
          }, Error, "not-subscribed");
          done();
        });

        it("insert a comment", function(done){
          const commentAdded = faker.lorem.sentence();
          const context = { userId : userID };
          const comment = faker.lorem.sentence();
          var args = { sessionDateID: sessionDateID, comment: commentAdded };
          insertComment._execute(context, args);

          assertCommentAdded(sessionDateID, commentAdded);
          done();
        });

        it("do not insert an empty comment", function(done){
          const context = { userId : userID };

          assert.throws( function(){
            insertComment._execute(context, { sessionDateID: sessionDateID });
          }, Error, "Comment is required");

          assert.throws( function(){
            insertComment._execute(context, { sessionDateID: sessionDateID, comment: "" });
          }, Error, "Comment is required");

          assert.throws( function(){
            insertComment._execute(context, { sessionDateID: sessionDateID, comment: " " });
          }, Error, "Comment is required");
          done();
        });
      });
    });

  });

}
