import { Mongo } from 'meteor/mongo';
import { Random } from 'meteor/random';
export const SessionDates = new Mongo.Collection('sessionDates');

SessionDates.publicFields = {
  "_id": 1,
  "sessionID" : 1,
	"date" : 1,
	"players" : 1,
	"comments" : 1,
  "last": 1,
  cancelled: 1
};

Factory.define('sessionDate', SessionDates, {
  sessionID(){ return Random.id(); },
	date(){ return faker.date.future(); },
	players : [],
	comments : [],
  last: true,
  cancelled: false
});
