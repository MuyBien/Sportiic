import { SessionDates } from '../sessionDates.js'
import { Sessions } from '../../sessions/sessions.js';

// User sessionDates
Meteor.publish('sessionDates.currents', function() {
  const userID = this.userId;
  const userSessions = Sessions.find({ "subscribers.userID": userID }, { "_id": 1 }).fetch();
  const userSessionsIds = _.pluck(userSessions, '_id');
  const userSessionDates = SessionDates.find({
    "sessionID": { $in: userSessionsIds },
    "last": true
  },{
    fields: SessionDates.publicFields
  });

  if(userSessionDates){
    return userSessionDates;
  }
  return this.ready();
});

// SessionDate Subscribe
Meteor.publish('sessionDates.subscribe', function(token) {
  const sessionToSubscribe = Sessions.findOne({ token: token });
  const sessionDateToSubscribe = SessionDates.find({
  	"sessionID": sessionToSubscribe._id,
    "last": true
  }, {
    fields: SessionDates.publicFields,
  });

  if(sessionDateToSubscribe){
    return sessionDateToSubscribe;
  }
  return this.ready();
});