import { SessionDates } from '../sessionDates/sessionDates.js';
import { Sessions } from '../sessions/sessions.js';
import { isSubscribed } from '../sessions/methods.js';
import { userSubscribedMixin, userRegisteredMixin, userUnregisteredMixin, cancelledSessionMixin, notCancelledSessionMixin } from '../mixins/mixins.js';
import { getUserName } from '../users/methods.js';
import { SessionDateManager } from './manager.js';

export const isRegistered = new ValidatedMethod({
  name: 'sessionDate.isRegistered',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    sessionDateID: { type: String },
    userID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ sessionDateID, userID }) {
    return SessionDates.find({
      _id: sessionDateID,
      "players.userID": userID
    }).count() > 0 ? true : false;
  }
});

export const registerPlayer = new ValidatedMethod({
  name: 'sessionDate.registerPlayer',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    sessionDateID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ sessionDateID }) {
    var currentUserID = this.userId;

    cancelledSessionMixin({ sessionDateID: sessionDateID });
    userSubscribedMixin({ sessionDateID: sessionDateID, userID: currentUserID });
    userRegisteredMixin({ userId: currentUserID }, { sessionDateID: sessionDateID, userID: currentUserID });

    SessionDates.update({
      _id: sessionDateID
    },{
      $push: { players: { userID: currentUserID, date: new Date() } }
    });

    if(Meteor.isServer && !Meteor.isTest){
      var sessionDate = SessionDates.findOne({ "_id": sessionDateID }, { "sessionID": 1 });
      var session = Sessions.findOne({ "_id": sessionDate.sessionID });
      let notification = new Notifier({
        sessionID: sessionDate.sessionID,
        userID: currentUserID,
        message: getUserName.call({ userID: currentUserID }) + " s'est inscrit pour la prochaine session",
        from: session.name,
        notificationType: "playerAdd"
      });
      notification.send();
    }
  }
});

export const unregisterPlayer = new ValidatedMethod({
  name: 'sessionDate.unregisterPlayer',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    sessionDateID: { type: String },
    userID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ sessionDateID, userID }) {
    userSubscribedMixin({ sessionDateID: sessionDateID, userID: userID });
    userUnregisteredMixin({ userId: userID }, { sessionDateID: sessionDateID, userID: userID });

    SessionDates.update({
      _id: sessionDateID
    },{
      $pull: { players: { userID: userID } }
    });

    if(Meteor.isServer && !Meteor.isTest){
      var sessionDate = SessionDates.findOne({ "_id": sessionDateID }, { "sessionID": 1 });
      var session = Sessions.findOne({ "_id": sessionDate.sessionID });
      let notification = new Notifier({
        sessionID: sessionDate.sessionID,
        userID: userID,
        message: getUserName.call({ userID: userID }) + " ne participe plus à la prochaine session",
        from: session.name,
        notificationType: "playerRemove"
      });
      notification.send();
    }
  }
});

export const insertComment = new ValidatedMethod({
  name: 'sessionDate.insertComment',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    sessionDateID: { type: String },
    date: {
      type: Date,
      autoValue: function() {
        return typeof date !== "undefined" ? date : new Date();
      }
    },
    comment: { type: String, min: 1 }
  }).validator({ clean: true, filter: false }),
  run({ sessionDateID, date, comment }) {
    var currentUserID = this.userId;

    userSubscribedMixin({ sessionDateID: sessionDateID, userID: currentUserID });

    var message = {
      author: currentUserID,
      text: comment,
      date: date
    };
    SessionDateManager.addComment({ id: sessionDateID, message: message });

    if(Meteor.isServer && !Meteor.isTest){
      var sessionDate = SessionDates.findOne({ "_id": sessionDateID }, { "sessionID": 1 });
      var session = Sessions.findOne({ "_id": sessionDate.sessionID });
      let notification = new Notifier({
        sessionID: sessionDate.sessionID,
        userID: currentUserID,
        message: getUserName.call({ userID: currentUserID }) + " : " + message.text,
        from: session.name,
        notificationType: "messageAdd"
      });
      notification.send();
    }
  }
});

export const cancelSessionDate = new ValidatedMethod({
  name: 'session.cancel',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    sessionDateID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ sessionDateID }) {
    const sessionDate = SessionDates.findOne({ _id: sessionDateID }, { "sessionID": 1 });
    const session = Sessions.findOne({ _id: sessionDate.sessionID });

    let rolesManager = new RolesManager();
    if( !rolesManager.isUserInRole({ userID: this.userId, role: ['admin', 'captain'], group: session._id }) ) {
      throw new Meteor.Error('notAllowed', "You are not allowed to access this method");
    }

    cancelledSessionMixin({ sessionDateID: sessionDateID });

    SessionDates.update({
      _id: sessionDate._id
    },{
      $set: { players: [], cancelled: true }
    });

    // Add system message
    var message = {
      author: this.userId,
      text: "a annulé la session",
      system: {
        type: "cancel"
      },
      date: new Date()
    };
    SessionDateManager.addComment({ id: sessionDate._id, message: message });

    if(Meteor.isServer && !Meteor.isTest){
      let notification = new Notifier({
        sessionID: sessionDate.sessionID,
        message: "La prochaine session de " + session.name + " est annulée !",
        from: session.name,
        notificationType: "sessionUpdate"
      });
      notification.send();
    }
  }
});

export const activateSessionDate = new ValidatedMethod({
  name: 'session.activate',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    sessionDateID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ sessionDateID }) {
    const sessionDate = SessionDates.findOne({ _id: sessionDateID }, { "sessionID": 1 });
    const session = Sessions.findOne({ _id: sessionDate.sessionID });

    let rolesManager = new RolesManager();
    if( !rolesManager.isUserInRole({ userID: this.userId, role: ['admin', 'captain'], group: session._id }) ) {
      throw new Meteor.Error('notAllowed', "You are not allowed to access this method");
    }

    notCancelledSessionMixin({ sessionDateID: sessionDateID });

    SessionDates.update({
      _id: sessionDate._id
    },{
      $set: { cancelled: false }
    });

    // Add system message
    var message = {
      author: this.userId,
      text: "a réactivé la session",
      system: {
        type: "cancel"
      },
      date: new Date()
    };
    SessionDateManager.addComment({ id: sessionDate._id, message: message });

    if(Meteor.isServer && !Meteor.isTest){
      let notification = new Notifier({
        sessionID: sessionDate.sessionID,
        message: "La prochaine session de " + session.name + " est réouverte !",
        from: session.name,
        notificationType: "sessionUpdate"
      });
      notification.send();
    }
  }
});
