import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { cancelSessionDate, registerPlayer } from '../methods.js';
import { Random } from 'meteor/random';

if (Meteor.isServer) {

  describe('sessionDates', function () {

    describe('methods', function(){

      const userID = Random.id();
      const sessionDateID = Random.id();
      const sessionDateCancelledID = Random.id();
      const sessionAdminID = Random.id();

      const createSessionDate = (props = {}) => {
        var sessionDateAdded = Factory.create('sessionDate', props);
        var sessionAdded = Factory.create('session', {
          _id: sessionDateAdded.sessionID,
          subscribers: [{ userID: userID }],
          sessionDates: [{ sessionDateID: sessionDateAdded._id }]
        });

        // Roles.setUserRoles( sessionAdminID, 'admin', sessionAdded._id );
      };

      before(function () {
        SessionDates.remove({});
        createSessionDate({
          _id: sessionDateID
        });
        createSessionDate({
          _id: sessionDateCancelledID,
          cancelled: true
        });
      });

      describe("cancel", function(){

        // it("cancel a sessionDate", function(done){
        //   const args = { sessionDateID: sessionDateID };
        //   cancelSessionDate._execute({ userId: sessionAdminID }, args);
        //   assert.isTrue(sessionDateAdded.cancelled === true);
        //   done();
        // });

        it("do not cancel if not connected", function(done){
          const args = { sessionDateID: sessionDateID };
          assert.throws( function(){
            cancelSessionDate._execute({}, args);
          }, Error, "notLogged");
          done();
        });

        it("do not cancel if the user do not have rigths", function(done){
          const args = { sessionDateID: sessionDateID };
          assert.throws( function(){
            cancelSessionDate._execute({ userId: userID }, args);
          }, Error, "notAllowed");
          done();
        });

        it("throws an error if trying to register to a cancelled sessionDate", function(done){
          const args = { sessionDateID: sessionDateCancelledID };
          assert.throws( function(){
            registerPlayer._execute({ userId: userID }, args);
          }, Error, "sessionDate-cancelled");
          done();
        });

        // it("add a comment to notify cancel", function(done){
        //   const args = { sessionDateID: sessionDateID };
        //   cancelSessionDate._execute({ userId: userID }, args);
        //   let sessionDate = SessionDates.findOne({ _id: sessionDateID });
        //   assert.isTrue( _.contains(_.pluck(sessionDate.comments, 'text'), "a annulé la session") );
        //   done();
        // });
      });
    });

  });

}
