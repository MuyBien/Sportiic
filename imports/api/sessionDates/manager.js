import { SessionDates } from './sessionDates.js';

/**
* Le SessionDateManager permet de manipuler les sessionDates en bases directement
**/
export const SessionDateManager = {

  addComment(doc){
    SessionDates.update(doc.id, {
      $push: { comments: doc.message }
    });
  }

};
