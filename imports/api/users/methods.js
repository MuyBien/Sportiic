
export const getUser = new ValidatedMethod({
  name: 'user.get',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    id: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ id }) {
    return Meteor.users.findOne({ _id: id });
  }
});

export const getUserName = new ValidatedMethod({
  name: 'user.getName',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    userID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ userID }) {
    //TODO test if user exists
    var user = Meteor.users.findOne({ _id: userID });
    if(user.username){
      return user.username;
    }
    if (user.services.google !== undefined) {
      return user.services.google.given_name;
    }
    if (user.services.facebook !== undefined) {
      return user.services.facebook.name;
    }
  }
});

export const getUserMail = new ValidatedMethod({
  name: 'user.getMail',
  validate: new SimpleSchema({
    userID: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ userID }) {
    //TODO test if user exists
    var user = Meteor.users.findOne({ _id: userID });
    if(user.emails){
      return user.emails[0].address;
    }
    if (user.services.google !== undefined) {
      return user.services.google.email;
    }
    if (user.services.facebook !== undefined) {
      return user.services.facebook.email;
    }
  }
});

export const updateUserName = new ValidatedMethod({
  name: 'user.updateName',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    username: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ username }) {
    var currentUserId = this.userId;
    if (Meteor.users.findOne({'username': username, id: { $ne: currentUserId }})) {
      throw new Meteor.Error("username-exists",
        "A user with this username already exists");
    }

    Meteor.users.update(currentUserId, {
        $set: {
            "username": username
        }
    });
  }
});

export const updateUserMail = new ValidatedMethod({
  name: 'user.updateMail',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    usermail: { type: String }
  }).validator({ clean: true, filter: false }),
  run({ usermail }) {
    var currentUserId = this.userId;
    if (Meteor.users.findOne({'emails.address': usermail, id: {$ne: currentUserId}})) {
      throw new Meteor.Error("usermail-exists",
        "A user using this mail already exists");
    }
    Meteor.users.update(currentUserId, {
        $set: {
            "emails": [{ address: usermail }]
        }
    });
  }
});

export const updateUserNotifications = new ValidatedMethod({
  name: 'user.updateNotifications',
  mixins : [LoggedInMixin],
  checkLoggedInError: {
    error: 'notLogged'
  },
  validate: new SimpleSchema({
    type: { type: String, allowedValues: ["sessionUpdate", "playerAdd", "playerRemove", "messageAdd"] },
    active: { type: Boolean }
  }).validator({ clean: true, filter: false }),
  run({ type, active }) {
    var notif = {};
    notif["profile.notifications." + type] = active;
    Meteor.users.update(this.userId, {
        $set: notif
    });
  }
});
