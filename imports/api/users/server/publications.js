import { Users } from '../users.js';
import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';

// Current user
Meteor.publish("users.me", function () {
  const currentUser = Meteor.users.find({
  	"_id": this.userId
  });

  if(currentUser){
	    return currentUser;
	}
	return this.ready();
});

// All users
Meteor.publish("users.all", function () {

  const userID = this.userId;
  const userSessions = Sessions.find({
  	"subscribers.userID": userID
  });

  let usersToSubscribe = [];
  userSessions.forEach( function(oneSession){
    usersToSubscribe = usersToSubscribe.concat( _.pluck(oneSession.subscribers, 'userID') );

    const userSessionDates = SessionDates.findOne({
      "sessionID": oneSession._id,
      "last": true
    });
    usersToSubscribe = usersToSubscribe.concat( _.pluck(userSessionDates.comments, 'author') );
  });

  const allUsers = Meteor.users.find({
    "_id": { $in: usersToSubscribe }
  }, {
    fields: Users.publicFields,
  });

  if(allUsers){
	   return allUsers;
	}
	return this.ready();
});

// All Roles
Meteor.publish("roles.all", function () {
    return Meteor.roles.find({})
});
