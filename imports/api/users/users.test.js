import { resetDatabase } from 'meteor/xolvio:cleaner';
import { PublicationCollector } from 'meteor/johanbrook:publication-collector';
import { Users } from '/imports/api/users/users.js';
import { Sessions } from '/imports/api/sessions/sessions.js';
import { SessionDates } from '/imports/api/sessionDates/sessionDates.js';
import { Random } from 'meteor/random';

import '../../api/users/server/publications.js';
import { getUser, getUserName, getUserMail, updateUserName, updateUserMail, updateUserNotifications } from '../../api/users/methods.js';

if (Meteor.isServer) {

  import './server/publications.js';

  describe('users', function () {

    const randomUser = () => {
      return {
        "username": faker.internet.userName(),
        "email": faker.internet.email(),
        "password": faker.internet.password()
      };
    };
    var userSession = {};
    var userSessionDate = {};
    const createUser = (props = {}) => {
      randomUser._id = Accounts.createUser(props);
      Sessions.update(
        { _id: userSession._id },
        { $push: { subscribers: { "userID": randomUser._id  }} }
      );
    };

    beforeEach(function () {
      Meteor.users.remove({});
      userSession = Factory.create('session');
      userSessionDate = Factory.create('sessionDate', { sessionID: userSession._id, last: true });
      _.times(3, () => createUser(randomUser()));
    });

    describe('publications', function () {

      describe('users.me', function () {

        it('send one user', function (done) {
          var meID = Meteor.users.findOne()._id;
          var meCollector = new PublicationCollector({ userId: meID });
          meCollector.collect('users.me', (collections) => {
            assert.equal(collections.users.length, 1);
            done();
          });
        });

      });

      describe('users.all', function () {

        it('send all users subscribed to the same session', function (done) {
          var meID = Meteor.users.findOne()._id;
          var usersCollector = new PublicationCollector({ userId: meID });
          usersCollector.collect('users.all', (collections) => {
            assert.equal(collections.users.length, 3);
            done();
          });
        });

        it('send a unsubscribed user that comment before leaving', function (done) {

          var commentatorID = Accounts.createUser({
            "username": faker.internet.userName(),
            "email": faker.internet.email(),
            "password": faker.internet.password()
          });
          SessionDates.update(userSessionDate._id, {
            $push: { comments: { author: commentatorID, text: "going to leave!", date: new Date() } }
          });

          var meID = Meteor.users.findOne()._id;
          var usersCollector = new PublicationCollector({ userId: meID });
          usersCollector.collect('users.all', (collections) => {
            assert.equal(collections.users.length, 4);
            done();
          });
        });

      });
    });

    describe('methods', function(){

      const getOneUser = () => {
        return Meteor.users.findOne();
      };

      describe('get', function(){
        it("return an user", function(done){
          var userID = getOneUser()._id;
          const context = { userId : userID };
          const args = { id: userID };
          var oneUser = getUser._execute(context, args);
          assert.equal(userID, oneUser._id);
          done();
        });

        it("throw an error if not connected", function(done){
          assert.throws( function(){
            getUser._execute({}, { id: Random.id() });
          }, Error, "notLogged");
          done();
        });
      });

      describe('getName', function(){
        it("return an userName", function(done){
          var randomUser = getOneUser(),
              userID = randomUser._id,
              userName = randomUser.username;
          oneUserName = getUserName._execute({ userId: userID }, { userID: userID });
          assert.equal(oneUserName, userName);
          done();
        });

        it("throw an error if not connected", function(done){
          assert.throws( function(){
            getUserName._execute({}, { userID: Random.id() });
          }, Error, "notLogged");
          done();
        });
      });

      describe('getMail', function(){
        it("return an userMail", function(done){
          var randomUser = getOneUser(),
              userID = randomUser._id,
              userMail = randomUser.emails[0].address;
          oneUserMail = getUserMail._execute({ userId: userID }, { userID: userID });
          assert.equal(oneUserMail, userMail);
          done();
        });
      });

      describe('updateUserName', function(){
        it("update current user name", function(done){
          var userID = randomUser._id,
              userName = randomUser.username,
              newUserName = faker.internet.userName();
          updateUserName._execute({ userId: userID }, { username: newUserName });
          oneUserName = getUserName._execute({ userId: userID }, { userID: userID });
          assert.equal(oneUserName, newUserName);
          done();
        });

        it("throws an error if not logged", function(done){
          assert.throws( function(){
            updateUserName._execute({}, { username: Random.id() });
          }, Error, "notLogged");
          done();
        });

        it("do not update if already exists", function(done){
          var userID = randomUser._id,
              newUserName = faker.internet.userName();

          createUser({ username: newUserName });
          assert.throws( function(){
            updateUserName._execute({ userId: userID }, { username: newUserName });
          }, Error, "username-exists");
          done();
        });
      });

      describe('updateUserMail', function(){
        it("update current user mail", function(done){
          var userID = randomUser._id,
              userMail = randomUser.email,
              newUserMail = faker.internet.email();
          updateUserMail._execute({ userId: userID }, { usermail: newUserMail });
          oneUserMail = getUserMail._execute({ userId: userID }, { userID: userID });
          assert.equal(oneUserMail, newUserMail);
          done();
        });

        it("throws an error if not logged", function(done){
          assert.throws( function(){
            updateUserMail._execute({}, { usermail: faker.internet.email() });
          }, Error, "notLogged");
          done();
        });

        it("do not update if already exists", function(done){
          var userID = randomUser._id,
              newUserMail = faker.internet.email();

          createUser({ email: newUserMail });
          assert.throws( function(){
            updateUserMail._execute({ userId: userID }, { usermail: newUserMail });
          }, Error, "usermail-exists");
          done();
        });
      });

      describe('updateUserNotifications', function(){

        const notificationsArr = ["sessionUpdate", "playerAdd", "playerRemove", "messageAdd"];
        const getNotificationObj = (notifType) => {
          return { type: notifType, "active": true };
        };
        const assertNotification = (notifType) => {
          var userID = randomUser._id;
          updateUserNotifications._execute({ userId: userID }, getNotificationObj(notifType));
          oneUser = getUser._execute({ userId: userID }, { id: userID });
          assert.isTrue(oneUser.profile.notifications[notifType]);
        };

        it("update notifications", function(done){
          notificationsArr.forEach( function(oneNotif){
            assertNotification(oneNotif);
          });
          done();
        });

        it("do not update unknown notification", function(done){
          assert.throws( function(){
            updateUserNotifications._execute({}, { type: "fakeType", "active": true });
          }, Error, "fakeType is not an allowed value [validation-error]");
          done();
        });

        it("throws an error if not logged", function(done){
          assert.throws( function(){
            updateUserNotifications._execute({}, { type: "messageAdd", "active": true });
          }, Error, "notLogged");
          done();
        });
      });

    });

  });

}
