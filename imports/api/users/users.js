export const Users = {};

Users.publicFields = {
	"_id": 1,
	"username": 1,

	"services.google.given_name": 1,
	"services.google.picture": 1,

	"services.facebook.id": 1,
	"services.facebook.name": 1,

	"profile": 1,
	"roles": 1
};
