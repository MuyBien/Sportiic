/*
* Permet de gérer les roles des utilisateurs sur les sessions
*/
RolesManager = class RolesManager {

  constructor(doc) {
    //
  }

  isUserInRole({ userID, role, group }) {
    return Roles.userIsInRole( userID, role, group );
  }

  setRole({ userID, role, group }) {
    return Roles.setUserRoles( userID, role, group );
  }

  addRole({ userID, role, group }){
    return Roles.addUserToRoles( userID, role, group );
  }

  removeRole({ userID, role, group }){
    return Roles.removeUsersFromRoles( userID, role, group );
  }

}
