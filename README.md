# README #
[ ![Meteor Version](https://img.shields.io/badge/meteor-1.4.2-blue.svg?style=flat)](https://www.meteor.com/)
[ ![Codeship Status for MuyBien/Sportiic](https://app.codeship.com/projects/b704a3c0-a354-0134-0500-26264aac178c/status?branch=master)](https://app.codeship.com/projects/190230)

### Sportiic ###

* Sportiic est une application en meteor.js. Elle permet d'organiser des sessions de sport facilement. Développé en Meteor, Sportiic est une application web.
* V0.6b

### Comment participer aux dev ###

* Installer Meteor.js
```
curl https://install.meteor.com/ | sh
```
Pour installer sur Windows (LoL), visiter https://www.meteor.com/install

* Copier ce répertoire
```
git clone git@gitlab.com:MuyBien/Sportiic.git
```
* Lancer meteor pour qu'il récupére les dépendances et lance le projet
```
cd sportiic/
meteor npm start
```

* Lancer les tests
Sportiic est couvert par des tests automatiques. Ils sont lancés à chaque push sur GitLab.
Il est possible de les lancer en local avec les commandes suivantes :
```
meteor npm test
// ou
meteor npm test-watch
```
